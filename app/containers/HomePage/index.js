/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 *
 */

import React, { useEffect, useRef } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { getMoviesList } from './redux/HomePage';
import 'bootstrap/dist/css/bootstrap.min.css';
import './homePage.css';
import MovieList from '../../components/movieList';
import MovieListHeading from '../../components/movieListHeading';

export default function HomePage() {
  const dispatch = useDispatch();
  const innerRef = useRef(null);
  let num = 1;
  const moviesList = useSelector(state => state.movies.moviesList);

  useEffect(() => {
    dispatch(getMoviesList(num));
    // subscribe event
    window.addEventListener('scroll', handleOnScroll);
    return () => {
      // unsubscribe event
      window.removeEventListener('scroll', handleOnScroll);
    };
  }, []);

  const handleOnScroll = () => {
    const windowHeight =
      'innerHeight' in window
        ? window.innerHeight
        : document.documentElement.offsetHeight;
    const { body } = document;
    const html = document.documentElement;
    const docHeight = Math.max(
      body.scrollHeight,
      body.offsetHeight,
      html.clientHeight,
      html.scrollHeight,
      html.offsetHeight,
    );
    const windowBottom = windowHeight + window.pageYOffset;
    const setBottomOffset = docHeight;
    if (windowBottom >= setBottomOffset) {
      if (
        moviesList &&
        moviesList.page &&
        moviesList.total_pages &&
        moviesList.total_pages >= moviesList.page
      ) {
        num += 1;
        dispatch(getMoviesList(num));
      } else {
        num += 1;
        dispatch(getMoviesList(num));
      }
    }
  };

  return (
    <div className="container-fluid" ref={innerRef}>
      <div className="row align-items-center">
        <MovieListHeading heading="Pop Movies" />
      </div>
      {moviesList && moviesList.results ? (
        <MovieList movies={moviesList} />
      ) : null}
    </div>
  );
}
