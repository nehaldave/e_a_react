/* eslint-disable prefer-template */
/* eslint-disable no-console */
/* eslint-disable no-unused-vars */
/* eslint-disable no-param-reassign */
import { useDispatch, useSelector } from 'react-redux';
import { createSlice } from '@reduxjs/toolkit';
import { GetAPI } from '../../../utils/services/api';
import { API_KEY } from '../../../utils/services/env';
// Slice
const slice = createSlice({
  name: 'movies',
  initialState: {
    moviesList: {},
    movieDetial: {},
    enableLoader: false,
  },
  reducers: {
    moviesList: (state, action) => {
      if (action.payload.pageNumber === 1) {
        state.moviesList = action.payload.res;
      } else if (state && state.moviesList && state.moviesList.results) {
        state.moviesList.results = state.moviesList.results.concat(
          action.payload.res.results,
        );
        state.moviesList.page = action.payload.pageNumber;
      }
      // state.moviesList = action.payload;
    },
    movieDetialSuccess: (state, action) => {
      state.movieDetial = action.payload;
    },
    enableLoader: (state, action) => {
      state.enableLoader = true;
    },
    disableLoader: (state, action) => {
      state.enableLoader = false;
    },
  },
});
export default slice.reducer;
// Actions
const {
  moviesList,
  movieDetialSuccess,
  enableLoader,
  disableLoader,
} = slice.actions;

// eslint-disable-next-line consistent-return
export const getMoviesList = props => async dispatch => {
  try {
    // eslint-disable-next-line prefer-template
    dispatch(enableLoader(''));
    const url = 'popular?api_key=' + API_KEY + '&page=' + props;
    const res = await GetAPI(url);
    dispatch(disableLoader(''));
    return dispatch(moviesList({ res, pageNumber: props }));
  } catch (e) {
    return console.error(e.message);
  }
};
// eslint-disable-next-line consistent-return
export const getMovieDetails = props => async dispatch => {
  try {
    dispatch(enableLoader(''));
    // eslint-disable-next-line prefer-template
    const url = props + '?api_key=' + API_KEY + '&append_to_response=videos';
    const res = await GetAPI(url);
    dispatch(disableLoader(''));
    return dispatch(movieDetialSuccess(res));
  } catch (e) {
    return console.error(e.message);
  }
};
