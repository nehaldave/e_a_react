/* eslint-disable react/prop-types */
/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 *
 */

import React, { useState, useEffect } from 'react';
import { Redirect } from 'react-router';

import 'bootstrap/dist/css/bootstrap.min.css';
import '../HomePage/homePage.css';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import IconButton from '@material-ui/core/IconButton';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import Button from '@material-ui/core/Button';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';
import PlayCircleOutlineIcon from '@material-ui/icons/PlayCircleOutline';
import { useDispatch, useSelector } from 'react-redux';
import CircularProgress from '@material-ui/core/CircularProgress';
import { getMovieDetails } from '../HomePage/redux/HomePage';
import { IMAGE_BASE_URL, IMAGE_SIZE } from '../../utils/services/env';

export default function DetailesPage(props) {
  const dispatch = useDispatch();
  const movieId = props.location.state.movieData;
  const movieDetial = useSelector(state => state.movies.movieDetial);
  const isLoader = useSelector(state => state.movies.enableLoader);

  useEffect(() => {
    dispatch(getMovieDetails(movieId));
  }, []);
  const [redirectUrl, setRedirectUrl] = useState('');
  const handleMovieClick = () => {
    setRedirectUrl('/');
  };
  return (
    <div className="container">
      {redirectUrl ? (
        <Redirect
          to={{
            pathname: redirectUrl,
          }}
        />
      ) : null}
      <div className="row">
        <div className="header col-md-12">
          <IconButton
            style={{
              color: 'white',
              fontSize: 'medium',
            }}
          >
            <ArrowBackIcon onClick={() => handleMovieClick()} />
            <b> Movie details</b>
          </IconButton>
          <IconButton
            style={{
              color: 'white',
              float: 'right',
            }}
          >
            <MoreVertIcon />
          </IconButton>
        </div>
      </div>
      {isLoader ? (
        <div
          style={{
            width: '100%',
            paddingTop: '50%',
          }}
        >
          {' '}
          <CircularProgress style={{ marginLeft: '45%' }} />{' '}
        </div>
      ) : (
        <>
          <div className="row">
            <div className="movie-title col-md-12">
              {movieDetial.original_title}
            </div>
          </div>
          <div className="section-1">
            <div className="row">
              <div className="movie-details">
                <div className="col-md-3">
                  <img
                    src={IMAGE_BASE_URL + IMAGE_SIZE + movieDetial.poster_path}
                    alt="movie"
                  />
                </div>
                <div className="col-md-6">
                  <div className="movie-year">{movieDetial.release_date}</div>
                  <div className="movie-duration">
                    <em> {movieDetial.runtime} mins</em>
                  </div>
                  <div className="movie-detail-lower">
                    <div className="movie-rating">
                      <b>{movieDetial.vote_average}/10</b>
                    </div>
                    <Button
                      style={{
                        backgroundColor: '#746A64',
                        textTransform: 'unset',
                        color: 'white',
                        minWidth: '110%',
                        paddingTop: '6%',
                        paddingBottom: '6%',
                      }}
                      variant="contained"
                    >
                      Add to Favorites
                    </Button>
                  </div>
                </div>
              </div>
            </div>
            <div className="movie-description">{movieDetial.overview}</div>
            {movieDetial && movieDetial.videos && movieDetial.videos.results ? (
              <>
                <div className="trailer">TRAILERS</div>
                <Divider />

                <List component="nav" aria-label="main mailbox folders">
                  {movieDetial.videos.results.map(singleVideo => (
                    <ListItem
                      button
                      style={{
                        backgroundColor: '#fafafa',
                        marginTop: '10px',
                        color: '#746A64',
                      }}
                    >
                      <ListItemIcon>
                        <PlayCircleOutlineIcon />
                      </ListItemIcon>
                      <ListItemText primary={singleVideo.name} />
                    </ListItem>
                  ))}
                </List>
              </>
            ) : null}
          </div>
        </>
      )}
    </div>
  );
}
