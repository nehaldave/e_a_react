/* eslint-disable prefer-template */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable no-console */
/* eslint-disable prettier/prettier */
/* eslint-disable arrow-body-style */
/* eslint-disable no-unused-vars */
/* eslint-disable react/prop-types */
/* eslint-disable prettier/prettier */
import React, { useState, useEffect, useRef } from 'react';
// import { Link } from 'react-router-dom';
import { Redirect } from 'react-router';
// import history from '../utils/history';
import { IMAGE_BASE_URL, IMAGE_SIZE } from '../utils/services/env';
const MovieList = props => {
  const [redirectUrl, setRedirectUrl] = useState('');
  const [selectedMovieId, setSelectedMovieId] = useState();
  const handleMovieClick = movie => {
    setRedirectUrl('detailedPage/' + movie.id);
    setSelectedMovieId(movie.id);
  };

  return (
    <>
      {redirectUrl ? (
        <Redirect
          to={{
            pathname: redirectUrl,
            state: {
              movieData: selectedMovieId,
            },
          }}
        />
      ) : null}
      <div className="row">
        {props.movies.results.map((singleMovie, index) => (
          <div
            onClick={() => {
              handleMovieClick(singleMovie);
            }}
            className="col"
          >
            <img
              src={IMAGE_BASE_URL + IMAGE_SIZE + singleMovie.backdrop_path}
              alt="movie"
            />
          </div>
        ))}
      </div>
    </>
  );
};

export default MovieList;
