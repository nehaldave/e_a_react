/* eslint-disable prettier/prettier */
/* eslint-disable react/prop-types */
/* eslint-disable prettier/prettier */
import React from 'react';
import IconButton from '@material-ui/core/IconButton';
import MoreVertIcon from '@material-ui/icons/MoreVert';
const MovieListHeading = props => (
  <div className="header col-md-12">
    <IconButton
      style={{
        color: 'white',
        fontSize: 'medium',
      }}
    >
      <b>{props.heading}</b>
    </IconButton>
    <IconButton
      style={{
        color: 'white',
        float: 'right',
      }}
    >
      <MoreVertIcon />
    </IconButton>
  </div>
);
export default MovieListHeading;
