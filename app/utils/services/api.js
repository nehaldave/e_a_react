/* eslint-disable arrow-body-style */
import { API_URL } from './env';
export const GetAPI = url => {
  return fetch(API_URL + url, {
    method: 'GET',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    // credentials: 'include',
  })
    .then(response => response.json())
    .then(responseData => {
      return responseData;
    });
};

export const PostAPI = (url, Body) => {
  return (
    fetch(API_URL + url, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      credentials: 'include',
      body: JSON.stringify(Body),
    })
      .then(response => response.json())
      // eslint-disable-next-line arrow-body-style
      .then(responseData => {
        return responseData;
      })
  );
};
