export const API_URL = 'http://api.themoviedb.org/3/movie/';
export const API_KEY = 'You API KEY';
export const IMAGE_BASE_URL = 'https://image.tmdb.org/t/p/';
export const IMAGE_SIZE = 'w185';
