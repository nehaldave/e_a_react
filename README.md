## 1. Things need to do after cloning project

- check if node.js and npm is install or not.
- download and install node.js and react.js
- Run 'npm install' or 'yarn install'
- Run 'npm start' or 'yarn start'
- Application is running on 'http://localhost:3000'

## 2. Project Architecture

- app: We use the container/component architecture. containers/ contains React components which are connected to the redux store. components/ contains dumb React components which depend on containers for data.
- The server folder contains development and production server configuration files.
- For state management We have used @reduxjs/toolkit.
- app/utills/services: This folder contains the base api method in api.js and env.js where all the base url and api key is being kept.

## Notes:

- You can set your api key in env.js file export const API_KEY = 'Your Api Key here' and access the list of popular movies;
